# README #


### Čemu je ta repozitorij namenjen? ###

* Projektu igrica pri predmetu Programiranje 2
* Moja igrica: Jewel (priredba igre Bejeweled)

### Kaj je igrica Jewel? ###

Gre za priredbo popularne igre Bejeweled. Dano je 8x8 polje, ki je zapolnjeno z draguljčki 7 različnih vrst. Cilj je dobiti niz 3, 4 ali 5 zaporednih draguljčkov, ki potem izginejo, igralec dobi točke, nadomestijo pa jih novi, 
naključno izbrani dragulji. Več o igri si lahko preberete na: http://en.wikipedia.org/wiki/Bejeweled. 
Igri bo dodana tudi možnost računalniškega igralca, ki bo igral po strategiji, ki gleda en korak naprej in se odloči za optimalno potezo.

### Kako poženem program? ###

* Za delovanje programa potrebujete Python 3.4, ki mora imeti inštalirano knjižnico PIL
* Program se požene z datoteko dragulj_strategija_1_korak.py

### Na koga se lahko obrnem za več informacij? ###

* Mihaela Pušnik (mihaela.pusnik@student.fmf.uni-lj.si)