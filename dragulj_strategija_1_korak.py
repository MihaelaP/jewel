#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Mihaela'

from tkinter import *
import tkinter
import time
import random
#import numpy as np
from copy import deepcopy

master = Tk()

oblika_width = 60   # za draguljcek
oblika_height = 60   # za draguljcek
polje_stolpci = 8   # število stolpcev na polju
polje_vrstice = 8   # število vrstic na polju

stevilo_draguljev = 7

gem1 = 'gem1.png'
gem2 = 'gem2.png'
gem3 = 'gem3.png'
gem4 = 'gem4.png'
gem5 = 'gem5.png'
gem6 = 'gem6.png'
gem7 = 'gem7.png'

slike_draguljev = [gem1, gem2, gem3, gem4, gem5, gem6, gem7]

cifre_v_dragulj = {1: gem1, 2: gem2, 3: gem3, 4: gem4, 5: gem5, 6: gem6, 7: gem7}
dragulj_v_cifro = {gem1: 1, gem2: 2, gem3: 3, gem4: 4, gem5: 5, gem6: 6, gem7: 7}
# slovar, da bomo lahko naredili random postavitev na polju


def get_key(item):
    # naredimo si ključ s katerim bomo sortirali najboljše poteze
    return item[0][1]

# Zgolj za lepši izpis, ko si printamo
def izpisi(matrika):
    return matrika
    #return np.array(matrika)


def poisci_trojke(matrika):
        # vrne par (trojke, polja), trojke  = št trojk, ki jih pobrišemo,
        # polja so množica koordinat, ki jih je treba brisat
        polja = set()
        trojke = 0
        for l, i in enumerate(matrika):  # gre po vrsticah v matriki, čekira, če se vsaj 3 cifre zapored ujemajo
            # najprej čekira ujemanje v vrstici
            for j in range(len(i)-2):
                if i[j] == i[j+1] and i[j] == i[j+2]:
                    trojke += 1
                    polja.add((j, l))
                    polja.add((j+1, l))
                    polja.add((j+2, l))
        for i, k in enumerate(matrika):  # gre po stolpcih v matriki, čekira, če se vsaj 3 cifre zapored ujemajo
            # najprej čekira ujemanje v vrstici
            if i < len(matrika) - 2:
                for j in range(len(k)):
                    if k[j] == matrika[i+1][j] and k[j] == matrika[i+2][j]:
                        trojke += 1
                        polja.add((j, i))
                        polja.add((j, i+1))
                        polja.add((j, i+2))
        return trojke, polja


class Dragulj():

    def __init__(self, root):
        self.sw = root.winfo_screenwidth() #preberem dimenzije ekrana, da lahko kasneje centriram okna
        self.sh = root.winfo_screenheight()
        self.tocke = 0
        self.zanka_racunalnik_id = None
        self.stevec = 0
        self.poteza = IntVar(0)
        self.dovoljene_poteze = 20
        self.skupne_tocke = IntVar(0)
        self.igralec = StringVar()
        self.dragulji = []  # naredimo si nek prazen seznam
        self.root = root

        root.title("Jewel")

        # Okvir za izbiro igralca
        self.okvir_vhodni = Frame(root, bg="#FFB266", padx=100, pady=100)
        self.okvir_vhodni.grid(column=1, row=1, sticky=W+E+N+S)

        label_vhodni = Label(self.okvir_vhodni, text="JEWEL\n Izberi igralca:",
                             font=("Franklin Gothic Heavy", 30), bg="#FFB266", fg="#004C99")
        label_vhodni.grid(column=3, row=0)

        label_kr_neki_1 = Label(self.okvir_vhodni, height=7, bg="#FFB266")
        label_kr_neki_1.grid(column=3, row=1)

        # Gumba za določitev igralca
        gumb_igralec = Button(self.okvir_vhodni, text="Uporabnik", font=("Franklin Gothic Heavy", 24),
                              bg="#004C99", fg="#FFB266", command=lambda:self.igralec_uporabnik())
        gumb_igralec.grid(column=3, row=3, sticky=W+E)

        gumb_racunalnik = Button(self.okvir_vhodni, text="Računalnik", font=("Franklin Gothic Heavy", 24),
                                 bg="#004C99", fg="#FFB266", command=lambda:self.igralec_racunalnik())
        gumb_racunalnik.grid(column=3, row=4, sticky=W+E)

        # Igralno polje
        self.okvir = Frame(root, bg="#004C99", padx=10, pady=10)

        label_kr_neki_2 = Label(self.okvir, width=1, bg="#004C99")
        label_kr_neki_2.grid(column=4, row=1)

        label_kr_neki_3 = Label(self.okvir, height=1, bg="#004C99")
        label_kr_neki_3.grid(column=0, row=10)

        # samo nastavimo Label
        self.konec_igre = Label(self.okvir, height=1)


        # Naredimo Meni
        glavni_meni=Menu(root)
        game_meni=Menu(glavni_meni,tearoff=0) #tearoff je da se ne da stran odtrgat menija
        glavni_meni.add_cascade(menu=game_meni,label='Igra')
        game_meni.add_command(label='{0}'.format('Nova igra'),command=self.nova_igra)
        game_meni.add_separator()  #po potrebi
        game_meni.add_command(label='{0}'.format('Izhod'),command=self.zapri_okno)

        # Meni, ki vsebuje informacije o igri, poimenujemo ga help_meni,
        # ker je v njem tudi opis igre, če je kdo ne pozna
        help_meni=Menu(glavni_meni,tearoff=0)
        glavni_meni.add_cascade(menu=help_meni,label='Dodatno')
        help_meni.add_command(label='{0}'.format('O igri'),command=self.dodatno)
        help_meni.add_separator()
        help_meni.add_command(label='Avtor',command=self.about)

        root.config(menu=glavni_meni)


        self.moves = list(self.vse_poteze())

        # Narišemo si polje
        self.glavno_polje = Canvas(self.okvir, width=oblika_width*polje_stolpci + polje_stolpci,
                                   height=oblika_height*polje_vrstice+polje_vrstice,
                                   bg="#FFB266")
        self.glavno_polje.grid(column=2, row=1, rowspan=8, columnspan = 8)

        self.mreza() #narišemo mrežo

        # naredimo prazno matriko, potem se bodo not naložile cifre, ki predstavljajo tip dragulja
        self.matrika = []
        for i in range(8):
            cif = []
            for j in range(8):
                cif.append(None)
            self.matrika.append(cif)

        #tu se dragulji narišejo na glavno_polje
        self.matrika_id = []
        for j in range(8):
            s = []
            for i in range(8):
                s.append(None)
            self.matrika_id.append(s)
        self.pravilno_zapolni()  # ta funkcija napolni polje z dragulji tako, da na začetku ni nobenih trojk
        self.tocke = 0

        # povežemo funkcijo z dogodkom - klikom na levi gumb
        self.glavno_polje.bind('<ButtonPress-1>', self.klik2)

        # Dodani labeli
        Label(self.okvir, text="Jewel",
              padx=5, pady=10,
              font=('Helvetica', 24, 'bold', 'underline'), fg="#FFB266", bg="#004C99").grid(column=5, row=0)
        Label(self.okvir, text="Tocke:",
              padx=5, pady=5,
              font=('Helvetica', 20, 'bold'), fg="#FFB266", bg="#004C99").grid(column=1, row=2, sticky=S+W+E)
        Label(self.okvir, textvariable = self.skupne_tocke,
              padx=5, pady=5,
              font=('Helvetica', 20, 'bold'), fg="#FFB266", bg = "#004C99").grid(column=1, row=3, sticky=N+W+E)
        Label(self.okvir, text='Igralec:',
              padx=5, pady=5,
              font=('Helvetica', 20, 'bold'), fg="#FFB266", bg = "#004C99").grid(column=1, row=0)
        Label(self.okvir, textvariable=self.igralec,
              padx=5, pady=5,
              font=('Helvetica', 20, 'bold'), fg="#FFB266", bg = "#004C99").grid(column=1, row=1)
        Label(self.okvir, text="Skupaj potez:",
              padx=5, pady=5,
              font=('Helvetica', 16, 'bold', 'italic'), fg="#FFB266", bg="#004C99").grid(column=1, row=6, sticky=S+W+E)
        Label(self.okvir, textvariable = self.poteza,
              padx=5, pady=5,
              font=('Helvetica', 16, 'bold', 'italic'), fg="#FFB266", bg="#004C99").grid(column=1, row=7, sticky=N+W+E)

    def about(self):  # okno o avtorju igre
        okno1=Tk()
        okno1.title('Avtor')
        s='Jewel\n Made by Mihaela Pušnik\n Copyright '+chr(169)+ ' 2015'
        besedilo=Label(okno1,text=s,borderwidth=20)
        besedilo.grid(row=0)
        gumb=Button(okno1,text='Ok',command=okno1.destroy)
        gumb.grid(row=1)
        uw=okno1.winfo_reqwidth()  # preberemo dimenzije okna, vendar niso čisto prave
        uh=okno1.winfo_reqheight()
        tw=int((self.sw-uw)/2)  # centriramo okno na ekranu
        th=int((self.sh-uh)/2)
        okno1.geometry('+'+str(tw)+'+'+str(th))  # popravimo samo postavitev
        okno1.mainloop()

    def dodatno(self):  # okno za pomoč
        okno1=Tk()
        okno1.title('O igri')  # sledi opis igre
        s = 'Jewel je igra, ki temelji na priljubljeni igri Bejeweled.\n' \
            'Igralec skuša v svoji potezi tvoriti čimdaljši niz enakih draguljev.\n' \
            'Potezo lahko izvede samo v primeru, da ta povzroči nastanek kakšnega\n' \
            'niza vsaj 3 ujemajočih se draguljev. Ko igralec tvori niz, dobi točke,\n' \
            'niz izgine, dragulji nad njim se pomaknejo nižje, na vrhu pa se pojavijo novi,\n' \
            'naključno izbrani dragulji. Za igro obstajajo različne strategije in težko\n' \
            'je določiti, katera bi bila optimalna. Za možnost, ko je igralec Računalnik,\n' \
            'je izbrana strategija, pri kateri računalnik gleda koliko potez je možnih\n' \
            'na polju, in koliko jih bo možnih po opravljeni posamezni potezi.\n' \
            'Tako določi svojo optimalno potezo.\n' \
            'Če na polju ni več možnih potez, je igra končana.\n' \
            'Če se igralcu zdi, da ni več možnih potez, naj klikne na poljuben dragulj.\n' \
            ' Če res ni več požnih potez, se bo pojavil napis "Konec igre." '
        besedilo=Label(okno1,text=s,borderwidth=20,anchor=NW)
        besedilo.grid(row=0)
        gumb=Button(okno1,text='Ok',command=okno1.destroy)
        gumb.grid(row=1)
        uw=okno1.winfo_reqwidth()  # preberem dimenzije okna, vendar niso povsem prave
        uh=okno1.winfo_reqheight()
        tw=int((self.sw-uw)/2)  # centriram okno na ekranu
        th=int((self.sh-uh)/2)
        okno1.geometry('+'+str(tw)+'+'+str(th))  # popravim samo postavitev
        okno1.mainloop()

    def drug_okvir(self, okvir1, okvir2):
        # Funkcija za prehajanje med okvirji
        okvir1.grid_forget()
        okvir1.update_idletasks()
        okvir2.grid(column=0, row=0, sticky=W+E+N+S)
        okvir2.update_idletasks()

    def zapri_okno(self):
        self.root.quit()

    def izprazni_matrike(self):
        #  vse matrike izprazni
        self.dragulji = []  # da nimamo preveč stvari v seznamu
        self.matrika = []
        for i in range(8):
            cif = []
            for j in range(8):
                cif.append(None)
            self.matrika.append(cif)
        self.matrika_id = []
        for j in range(8):
            s = []
            for i in range(8):
                s.append(None)
            self.matrika_id.append(s)

    def pravilno_zapolni(self):
        # funkcija, ki polje na začetku zapolni tako, da ni nobenih trojk
        trojke, _ = poisci_trojke(self.matrika)
        while trojke > 0:
            self.pobrisi_trojke()
            self.spusti_dragulje()
            trojke, _ = poisci_trojke(self.matrika)

    def dodaj_tocke(self, trojke):
        # povečaj točke za dano št trojk
        # na zaslonu prikaži nove točke
        self.tocke += trojke*10
        return self.tocke

    def klik(self, x, y):
        # Ko kliknemo na prvi dragulj, se okrog njega nariše kvadratek, ko kliknemo na
        # njegovega soseda, se ta kvadratek izbriše, dragulja pa se zamenjata.
        # dragulj je določen s koordinatami x,y
        vrstica = y // 60
        stolpec = x // 60
        self.stevec += 1
        self.dovoljene_poteze -= 1
        if self.stevec % 2 == 1:
            self.pravokotnik = self.glavno_polje.create_rectangle(x+60, y+60, x, y, outline = 'red',
                                           width = 3)
            self.vrstica, self.stolpec = vrstica, stolpec
        else:
            self.glavno_polje.delete(self.pravokotnik)
            # s spodnjim v matriki_draguljev zamenjamo dragulja
            self.klik1(self.stolpec, self.vrstica, stolpec, vrstica)

    def klik1(self, x1, y1, x2, y2):
        # malo bolj splošni klik, ki ga potem uporabimo tudi pri igri računalnika
        if abs(y1 - y2) + abs(x1 - x2) == 1:
                        # s tem (zgornjim) dovolimo le menjave
                        # draguljčkov, ki so sosednji, tj imajo skupno stranico
            self.matrika[y2][x2], self.matrika[y1][x1] = \
                self.matrika[y1][x1], self.matrika[y2][x2]
            trojke, polja = poisci_trojke(self.matrika)
            if trojke == 0:
                # poteza ni dovoljena
                self.matrika[y2][x2], self.matrika[y1][x1] = \
                    self.matrika[y1][x1], self.matrika[y2][x2]
            else:
                # poteza je dovoljena
                self.poteze()
                # zamenjamo v matriki_id
                self.matrika_id[y2][x2], self.matrika_id[y1][x1] = \
                    self.matrika_id[y1][x1], self.matrika_id[y2][x2]
                # zamenjamo na zaslonu
                # ker smo v matriki že zamenjali, damo koordinate kot spodaj, da ne menjamo nazaj
                self.glavno_polje.coords(self.matrika_id[y2][x2], (x2*60 + 30, y2*60 + 30))
                self.glavno_polje.coords(self.matrika_id[y1][x1], (x1*60 + 30, y1*60 + 30))
                self.glavno_polje.update_idletasks()  # takoj prikaže spremembo na zaslonu
                # najdemo in pobrišemo trojke
                self.pobrisi_trojke()
                # spustimo dragulje
                self.glavno_polje.after(100, self.spusti_dragulje())
                self.glavno_polje.after(100, self.pravilno_zapolni())

    def klik2(self, event):
        # v returnu moramo imeti malo zamaknjene koordinate eventa, da bo res obrobil kvadratek
        if self.igralec.get() == 'Uporabnik':
            if event.x < 0 or event.y < 0 or event.x > 480 or event.y > 480:
                pass
            if self.ni_moznih_potez_uporabnik():
                # če ni več možnih potez, se pod glavnim poljem (pod Canvasom) pojavi napis 'Konec igre'
                self.konec_igre = Label(self.okvir, height=1, text="Konec igre\n",
                                 font=("Franklin Gothic Heavy", 30), bg="#FFB266", fg="#004C99")
                self.konec_igre.grid(column=2, row=12, columnspan=8)
                self.konec_igre.config(text='Konec igre')
            else:
                return self.klik(event.x - (event.x % 60), event.y - (event.y % 60))

        pass  # ne moreš klikat po canvasu, ko računalnik igra

    def izbrisi_polje(self, p):
        # pobriše dragulj na polju p
        # ko pobriše dragulje, mora tudi trojke in polja v funkciji poisci_trojke resetirat
        y, x = p[0], p[1]
        self.matrika[x][y] = None
        a = self.matrika_id[x][y]
        self.matrika_id[x][y] = None
        self.glavno_polje.delete(a)

    def pobrisi_trojke(self):
        # Funkcija, ki najprej poišče vse trojke, ki so trenutno na polju, jih izbriše,
        # doda ustrezno število točk in resetira trojke in polja
        trojke, polja = poisci_trojke(self.matrika)
        self.dodaj_tocke(trojke)
        self.skupne_tocke.set(0)
        self.skupne_tocke.set(self.tocke)
        #self.skupne_tocke.set(0)
        for p in polja:
            self.izbrisi_polje(p)
        trojke = 0
        polja = set()

    def spusti_dragulje(self):
        # Funkcija, ki 'spusti' dragulje in dopolni polje
        for j in range(8):
            i = 7  # se ustavi, ko najdemo None
            k = 7 # ta išče prvega, ki ni None
            while i >= 0:
                if self.matrika[i][j] is None:
                    while k >= 0 and self.matrika[k][j] is None:
                        k -= 1
                    if k >= 0:
                        self.matrika[i][j] = self.matrika[k][j]
                        self.matrika_id[i][j] = self.matrika_id[k][j]
                        self.matrika_id[k][j] = None
                        self.matrika[k][j] = None
                        self.glavno_polje.coords(self.matrika_id[i][j], j*60 + 30, i*60 + 30)
                    else:
                        self.matrika[i][j] = dragulj_v_cifro[random.choice(slike_draguljev)]
                        self.slika = tkinter.PhotoImage(file=str(cifre_v_dragulj[self.matrika[i][j]]))
                        self.dragulji.append(self.slika)
                        self.matrika_id[i][j] = self.glavno_polje.create_image(j*60 + 31, i*60 + 31, image=self.slika)
                i -= 1
                k -= 1

    def poteze(self):  # šteje poteze
        self.poteza.set(self.poteza.get() + 1)
        return self.poteza

    def mreza(self):
        # risemo mrezo
        for i in range(oblika_width + 1, oblika_width * polje_stolpci + oblika_width, oblika_width):
            self.glavno_polje.create_line(i, 0, i, oblika_width*polje_stolpci)
            self.glavno_polje.create_line(0, i, oblika_width*polje_stolpci, i)

    def nova_igra(self):
        self.mreza()
        self.drug_okvir(self.okvir, self.okvir_vhodni)
        self.izprazni_matrike()
        self.tocke=0
        self.skupne_tocke.set(self.tocke)
        self.glavno_polje.update_idletasks()
        self.poteza.set(0)
        self.konec_igre.config(bg="#004C99")
          #počakamo, da self.mislec konča
        if self.zanka_racunalnik_id is not None:
            self.glavno_polje.after_cancel(self.zanka_racunalnik_id) # zdaj se prekine zanka

    def ni_moznih_potez_uporabnik(self):
        # za uporabnika preveri če še ima kaj možnih potez
        dovoljene = self.poisci_dovoljene_poteze(self.matrika)
        a = []
        for move in dovoljene:
            a.append(move)
        if a == []:
            return True
        else:
            return False

    def igralec_uporabnik(self):
        self.drug_okvir(self.okvir_vhodni, self.okvir)
        self.igralec.set('Uporabnik')
        self.pravilno_zapolni()
        self.tocke = 0
        self.skupne_tocke.set(0)
        self.poteza.set(0)
        self.glavno_polje.update_idletasks()

    def dobi_potezo(self):
        a = self.najboljsa_poteza(self.matrika)
        self.najdena_poteza = a
        self.mislec = None

    def igralec_racunalnik(self):
        self.drug_okvir(self.okvir_vhodni, self.okvir)
        self.pravilno_zapolni()
        self.tocke = 0
        self.skupne_tocke.set(0)
        self.poteza.set(0)
        self.glavno_polje.update_idletasks()
        self.igralec.set('Računalnik')
        self.pravilno_zapolni()
        self.zanka_racunalnik()


    def zanka_racunalnik(self):
        mozna_poteza = self.poteza_racunalnik()
        self.glavno_polje.update_idletasks()
        if mozna_poteza == False:
            return
        else:
            self.poteza_racunalnik()
            self.glavno_polje.update_idletasks()
            if mozna_poteza == False:
                return
            else:
                self.zanka_racunalnik_id = self.glavno_polje.after(100, self.zanka_racunalnik)


    def poteza_racunalnik(self):
        if self.najboljsa_poteza(self.matrika) is None:
            self.konec_igre = Label(self.okvir, height=1, text="Konec igre\n",
                             font=("Franklin Gothic Heavy", 30), bg="#FFB266", fg="#004C99")
            self.konec_igre.grid(column=2, row=12, columnspan=8)
            self.konec_igre.config(text='Konec igre')
            return False
            #quit()  # če želimo, da se okno zapre, ko se igra konča, torej ko zmanjka potez
        else:
            a, b = self.najboljsa_poteza(self.matrika)
            x1, y1 = a
            x2, y2 = b
            self.klik1(x1,y1,x2,y2)


    # Funkcije za strategijo računalnika
    # Večina spodnjih funkcij mora dobiti kot argument tudi grid, ne le self.
    # Razlog za to je, da bomo gledali različne položaje, v self.matriki pa nočemo prevečkrat menjavati.

    def vse_poteze(self):
        # vrne seznam vseh potez na polju, tudi če poteze niso možne
        # vrstice:
        for x in range(polje_stolpci - 1):
            for y in range(polje_vrstice):
                # zamenja sosednja draguljčka
                yield (x,y), (x + 1, y)
        for x in range(polje_stolpci):
            for y in range(polje_vrstice - 1):
                # zamenjaš s spodnjim draguljčkom
                yield (x, y), (x, y + 1)

    def zamenjana_pozicija(self, grid, a, b):
        # vrne polje, kjer sta draguljčka a in b zamenjana
        # delamo s kopijo matrike, ker nočemo dejansko menjati v matriki
        a_x, a_y = a
        b_x, b_y = b
        novo_polje = deepcopy(grid)  # deepcopy je za večnivojske sezname
        s = novo_polje[a_y][a_x]
        novo_polje[a_y][a_x] = novo_polje[b_y][b_x]
        novo_polje[b_y][b_x] = s
        return novo_polje

    def poisci_dovoljene_poteze(self, grid):
        # poišče poteze, ki bodo dale niz vsaj 3 draguljev
        # zato si mora pogledati kaj je po premiku
        for move in self.moves:
            move_a, move_b = move
            po_premiku1 = self.zamenjana_pozicija(grid, move_a, move_b)
            trojke, polja = poisci_trojke(po_premiku1)
            if trojke > 0:
                yield move

    def po_premiku(self, grid, move):
        # vrne stanje polja po premiku move
        move_a, move_b = move
        grid = self.zamenjana_pozicija(grid, move_a, move_b)
        for j in range(8):
            i = 7  # se ustavi, ko najdemo None
            k = 7 # ta išče prvega, ki ni None
            while i >= 0:
                if grid[i][j] is None:
                    while k >= 0 and grid[k][j] is None:
                        k -= 1
                    if k >= 0:
                        grid[i][j] = grid[k][j]
                        grid[k][j] = None
                i -= 1
                k -= 1
        return grid

    def najboljsa_poteza(self, grid):
        # Pogleda vse poteze, ki bodo uničile nekaj draguljev in vrne najboljšo izbiro
        dovoljene_poteze = self.poisci_dovoljene_poteze(grid)
        # pogleda vse možne poteze in poiščetiste, ki imajo potem največ možnih potez
        max_prihodnje_poteze = -1  # vsaka možna poteza bo mela več kot to
        best_poteze = []
        neki = []
        for move in dovoljene_poteze:
            a = self.poisci_dovoljene_poteze(self.po_premiku(grid, move))
            prihodnje_poteze = len(list(self.poisci_dovoljene_poteze(self.po_premiku(grid, move))))
            if prihodnje_poteze > max_prihodnje_poteze:
                max_prihodnje_poteze = prihodnje_poteze
                best_poteze = [move]
                neki = [max_prihodnje_poteze]
            elif prihodnje_poteze == max_prihodnje_poteze:
                best_poteze.append(move)
                neki.append(max_prihodnje_poteze)
        if not best_poteze:
            return None
        # poteze je treba še razvrstiti, da tiste, ki so višje na polju izvedemo prej.
        # Če bi radi gledali še eno potezo naprej, bi bilo dovolj gledat koliko prihodnjih potez je še možnih.
        # Ni dobro gledati še eno potezo naprej, ker se lahko zgodi, da v prvem krogu dobimo samo eno možno potezo,
        # jo izvedemo, potem pa v naslednjem koraku sploh ni več možnih potez
        best_poteze.sort(key=get_key)
        return best_poteze[0]


app = Dragulj(master)
master.mainloop()


